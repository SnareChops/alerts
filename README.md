### Donation Alert Variables

{name} - Name of tipper
{amount} - Amount of tip

### Follow Alert Variables

{name} - Name of follower

### Subscriber Alert Variables

{name} - Name of subscriber or gift recipient
{months} - Length of sub
{gifter} - Gifter

### Bit Alert Variables

{name} - Name of cheerer
{amount} - Amount of bits

### Host Alert Variables

{name} - Name of hoster
{count} - Number of viewers

### Raid Alert Variables

{name} - Name of raider
{count} - Number of viewers
